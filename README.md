# README #

Demo docker-compose, to control 1 nginx frontend, with 2 php backends.

### What is this repository for? ###

This repo can be used to setup a single nginx web frontend, with two php backends.
The myphp image is build from "phusion", the nmcteam/php56 image is a php5.6 image available from docker.
The nxing image is build from "phusion".

the docker-compose.yml is then used to link and start them together.

Two vhosts are used, docker.dev, and docker2.dev, they contain different fastcgi pass directives to pass php execution to the different backends.

### How do I get set up? ###

- 1 build images.

cd images/myphp

docker build . -t myphp

cd images/nginx

docker build . -t nginx

- 2 download the nmcteam/php56 image

docker pull nmcteam/php56

List your local images with:

docker images

to resolve the docker.dev and docker2.dev records, add them to your local /etc/hosts file pointing to your local ip. Like so:

<my_local_ip>   docker.dev
<my_local_ip>   docker2.dev


Starting:
In the dir containing the docker-compose.yml (root in this repo) execute:
docker-compose up -d

Using:
open the url docker.dev:8080 take a note of the hostname (first line)
open the url docker2.dev:8080 and take a note of the hostname

They should be different, indicating that by arriving on the nginx via a different vhost, it will execute a different php fastcgi backend. hence he difference in hostnames.
This enables (for example) to update on of the php backends to a different php version, and then test the same code by simply visiting it through different vhost names.

Stopping:
docker-compose down

Connecting to troubleshoot a running container with a bash shell:
docker ps (to locate the container ID)

docker exec -it <container-ID> bash


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Richard van Beers
Martin Kaberg